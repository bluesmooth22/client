AlertReportTimeInterval = 4*24*60*60 --学徒多长时间没有汇报成绩，师傅可以无惩罚的开除他
UserForbidTimeDuration = 1 --学徒离开师门或者师傅无故开除学徒时，禁止操作师徒系统的时间
CanAcceptStudent = 1

OneDayAcceptStudentMaxCnt = 5
KickStudentDayCnt = 0   --徒弟几天不在线，师傅可以无损开除
LeaveSchoolDayCnt = 0    --师傅几天不在线，徒弟可以无损离开

