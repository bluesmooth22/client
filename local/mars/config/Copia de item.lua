include("script/game_script_base.lua")

ItemMaxSmithingLevel                = 10;			        								-- 道具最大打造等级
ItemSmithing_FailCounter_Enable     = true;                 					-- 是否开启记录道具的打造失败次数
ItemSmithing_FailCounter_Add        = { 5,5,5,5,5,5,5,4,3,2,0 }   			-- 额外成功率(失败次数 * x%)
ItemSmithing_FailCounter_MaxAdd     = 100                    					-- 额外成功率最大不超过x%
ItemSmithing_TrumpEffect    	      = "eff_wlev10";                 	-- 主法宝达到一定等级时的特效
ItemSmithing_TrumpLink          	  = "root";                   			-- 主法宝达到一定等级时的特效Link点
ItemSmithing_ArmorEffect         	  = "eff_alev10";               		-- 防具达到一定等级时的特效
ItemSmithing_ArmorLink          	  = "root";                   			-- 防具达到一定等级时的特效Link点
ItemSmithing_PlusAttrib_Trump       = { 0,0,0,0,0,0,251,252,253,254 } -- 道具到达特定锻造等级后每级附加一条特殊属性
ItemSmithing_PlusAttrib_Armor       = { 0,0,0,0,0,0,261,262,263,264 } -- 道具到达特定锻造等级后每级附加一条特殊属性
ItemSmithing_FailCounter_Clear 			= { 1,1,1,1,1,1,1,1,1,1 } 				-- 打造成功后额外成功率是否清除掉
ItemSmithing_AddFailCounter 			= { 1,1,1,1,1,1,1,1,1,1 }   --打造失败增加的失败次数

CanSeeOtherEquipment                = true;
ItemEffectSmithingLevel							= 10;	        -- 道具达到该等级时增加的特效
ItemRiderEffect                     = "eff_ridelev10"   				-- 座骑达到一定等级时的特效
ItemRiderEffectLink                 = "root"      -- 座骑达到一定等级时的特效link点
ItemSmithing_AddFailCounter 			= { 1,1,1,1,1,1,1,1,1,1 }   --打造失败增加的失败次数

-- 系统初始化
function ItemInit()
    -- 初始化语言 
    LOAD_LAN("MSG_SMITHING_SUCCESS")
    LOAD_LAN("MSG_SMITHING_FAIL")
end


-- 锻造成功
function OnSmithingSuccess(user, item, origSmithingLevel, smithingLevel)
    ReceiveMsg(user, LAN("MSG_SMITHING_SUCCESS"), CHANNEL_SYS);
end


-- 锻造失败
function OnSmithingFail(user, item, origSmithingLevel, smithingLevel, costGold, failedNum)
    ReceiveMsg(user, LAN("MSG_SMITHING_FAIL"), CHANNEL_SYS);
    
    --if (origSmithingLevel >= 0 and origSmithingLevel < 3) then
    --    ReceiveMsg(user, LAN("MSG_SMITHING_FAIL"), CHANNEL_SYS);
    --elseif (origSmithingLevel >= 3 and origSmithingLevel < 6) then
    --    ReceiveMsg(user, LAN("msg_script_smithing_fail1"), CHANNEL_SYS);
    --elseif (origSmithingLevel >= 6 and origSmithingLevel < 10) then
    --    ReceiveMsg(user, LAN("msg_script_smithing_fail1"), CHANNEL_SYS);
    --end

    if(costGold >= 600000) then
				CreateItemAndMail(user, 3990, "white", "binded", 0, 0, 0, 0);
    elseif (costGold >= 300000) then
				CreateItemAndMail(user, 3989, "white", "binded", 0, 0, 0, 0);
    elseif (costGold >= 50000) then
				CreateItemAndMail(user, 4124, "white", "binded", 0, 0, 0, 0);
    end 
        
    if(origSmithingLevel==9 and failedNum>=10 and math.mod(failedNum,5)==0) then
		    if (GetItemTypeById(user, item) == 1) then
		    		CreateItemAndMail(user, 403, "white", "binded", 0, 0, 0, 0);
		    else
		    		CreateItemAndMail(user, 403, "white", "binded", 0, 0, 0, 0);
		    end
		end
end

function CanItemEnchant(nItemType, nGemItemIndex)
    if nItemType < 1 or nItemType > 8 then
        return 0
    end
    
    return 1
end
