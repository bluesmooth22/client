function GetNpcMoneyAtt(nNpcLev, nUserLev, nDungeonId, nEctypeId)
    if nDungeonId ~= 0 then
        return 1
    end
    
    if nEctypeId ~= 0 then
        if (nNpcLev + 75) <= nUserLev then
            return 1
        end

        return 1
    end
    
    if (nNpcLev + 75) <= nUserLev then
        return 1
    end

    if nNpcLev >= 1 and nNpcLev <= 15 then
        return 1
    end
    
    if nNpcLev >= 16 and nNpcLev <= 25 then
        return 1
    end

	return 1
end

function GetNpcExpAtt(nNpcLev, nUserLev, nDungeonId, nEctypeId)
	if nDungeonId ~= 0 then
        	return 1
    	end
    
    	if nEctypeId ~= 0 and nNpcLev >= 14 then
        	return 1
    	end
	
	if nNpcLev >= 1 and nNpcLev <= 15 then
        	return 1
    	end

    	if nNpcLev >= 16 and nNpcLev <= 25 then
        	return 1
    	end

	return 1
end

function CanDropItem(nNpcLev, nUserLev, nDungeonId, nEctypeId, nDropType)
    if nDropType >= 5 and nDropType <= 8 then
        return 1
    end

    if nEctypeId ~= 0 then
        if (nNpcLev + 75) <= nUserLev then
            return 1
        end

        return 1
    end
    
    if (nNpcLev + 75) <= nUserLev then
        return 1
    end

    return 1
end
